﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestDemo.Controllers
{
    public class StatusController : Controller
    {
        public string Index()
        {
            return "ACTIVE";
        }
    }
}
